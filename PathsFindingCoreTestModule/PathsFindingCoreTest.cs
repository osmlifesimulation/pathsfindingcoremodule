﻿using NetTopologySuite.Geometries;
using OSMLSGlobalLibrary.Map;
using OSMLSGlobalLibrary.Modules;
using OSMLSGlobalLibrary.Observable.Geometries;
using OSMLSGlobalLibrary.Observable.Geometries.Actor;
using PathsFindingCoreModule;

namespace PathsFindingCoreTestModule
{
	public class PathsFindingCoreTest : OSMLSModule
	{
		[CustomStyle(@"new style.Style({
                stroke: new style.Stroke({
                    color: 'rgba(90, 0, 157, 1)',
                    width: 2
                })
            });
        ")]
		private class Highlighted : GeometryCollectionActor
		{
			public Highlighted(Geometry[] geometries) : base(geometries)
			{
			}
		}

		protected override void Initialize()
		{
			// Works with https://www.openstreetmap.org/api/0.6/map?bbox=37.4622%2C55.7472%2C37.5193%2C55.7695
			var firstCoordinate = new Coordinate(4171958, 7509820);
			var secondCoordinate = new Coordinate(4173682, 7510147);

			MapObjects.Add(new Highlighted(new Geometry[]
			{
				new ObservableLineString(PathsFinding.GetPath(firstCoordinate, secondCoordinate, "Transport").Result.Coordinates),
				new ObservableLineString(PathsFinding.GetPath(firstCoordinate, secondCoordinate, "Walking").Result.Coordinates)
			}));
		}

		public override void Update(long elapsedMilliseconds)
		{
		}
	}
}