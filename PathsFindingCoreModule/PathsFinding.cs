﻿using System.Collections.Concurrent;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PathsFindingCoreModule
{
	public static class PathsFinding
	{
		private class PathRequest
		{
			public bool InProcess;
			public SemaphoreSlim Semaphore { get; }

			public Coordinate SourceNode { get; }
			public Coordinate TargetNode { get; }

			public LineString Path { get; set; }

			public PathRequest(SemaphoreSlim semaphore, Coordinate sourceNode, Coordinate targetNode)
			{
				Semaphore = semaphore;
				SourceNode = sourceNode;
				TargetNode = targetNode;
			}
		}

		private static readonly Dictionary<string, ConcurrentQueue<PathRequest>> PathRequests =
			new Dictionary<string, ConcurrentQueue<PathRequest>>();

		private const int AdditionalThreadsCount = 4;

		public static void AddGraph(Graph graph, string type)
		{
			for (var i = 0; i < AdditionalThreadsCount; i++)
			{
				PathRequests[type] = new ConcurrentQueue<PathRequest>();
				new Thread(() =>
				{
					while (true)
					{
						var search = new SearchEngine(graph.Nodes);
						if (PathRequests[type].TryDequeue(out var request))
						{
							//TODO: run this code in new Thread, but we need to create new instance of SearchEngine with new isolated List<Node> (we need to copy it and all nodes in all edges)
							//or maybe we can just wait until the same code is executed? ...and then create a new Thread

							request.InProcess = true;

							search.ChangeStartEnd(graph.GetClosestNode(request.SourceNode),
								graph.GetClosestNode(request.TargetNode));
							var path = search.GetShortestPathAstart();

							if (path.Count == 1) continue;

							request.Path = new LineString(path.Select(x => x.Coordinate.Copy()).ToArray());
							request.Semaphore.Release();
						}
						else
						{
							Thread.Sleep(100);
						}
					}
				}).Start();
			}
		}

		public static async Task<LineString> GetPath(Coordinate sourceNode, Coordinate targetNode, string graphType)
		{
			var semaphore = new SemaphoreSlim(0, 1);
			var pathRequest = new PathRequest(semaphore, sourceNode, targetNode);
			PathRequests[graphType].Enqueue(pathRequest);

			await semaphore.WaitAsync();
			return pathRequest.Path;
		}
	}
}