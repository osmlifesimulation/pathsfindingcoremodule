﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace PathsFindingCoreModule
{
	internal class CommonGraph : Graph
	{
		public CommonGraph(IEnumerable<LineString> roads)
		{
			var edges = new List<LineString>();
			foreach (var road in roads)
			{
				for (var i = 1; i < road.Coordinates.Length; i++)
				{
					var firstCoordinate = road.Coordinates[i - 1];
					var secondCoordinate = road.Coordinates[i];

					var firstNode = Nodes.FirstOrDefault(n => n.Coordinate.Equals(firstCoordinate));
					if (firstNode == null)
					{
						var newNode = new Node(firstCoordinate.Copy());
						Nodes.Add(newNode);
						firstNode = newNode;
					}

					var secondNode = Nodes.FirstOrDefault(n => n.Coordinate.Equals(secondCoordinate));
					if (secondNode == null)
					{
						var newNode = new Node(secondCoordinate.Copy());
						Nodes.Add(newNode);
						secondNode = newNode;
					}

					if (edges.Any(x =>
						x.Coordinates[0].Equals(firstCoordinate) &&
						x.Coordinates[1].Equals(secondCoordinate) ||
						x.Coordinates[1].Equals(firstCoordinate) &&
						x.Coordinates[0].Equals(secondCoordinate)
					)) continue;

					var newEdgeLine = new LineString(new[] {firstCoordinate, secondCoordinate});
					var newEdge = new Edge(firstNode, secondNode, 1 * newEdgeLine.Length);
					firstNode.Connections.Add(newEdge);
					secondNode.Connections.Add(newEdge);

					edges.Add(newEdgeLine);
				}
			}
		}

		public override Node GetClosestNode(Coordinate sourcePoint)
		{
			return Nodes.OrderBy(node => new LineString(new[] {node.Coordinate, sourcePoint}).Length)
				.First();
		}
	}
}