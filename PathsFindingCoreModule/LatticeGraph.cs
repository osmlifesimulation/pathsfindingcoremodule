﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace PathsFindingCoreModule
{
	internal class LatticeGraph : Graph
	{
		private readonly List<List<Node>> _NodesMatrix = new List<List<Node>>();

		private readonly List<Polygon> _BuildingPolygons;

		private const int GridFrequency = 10;

		private static IEnumerable<T> AdjacentBottomRightElements<T>(IReadOnlyList<List<T>> arr, int row, int column)
		{
			var rows = arr.Count;
			var columns = arr[0].Count;

			for (var j = row; j <= row + 1; j++)
			for (var i = column; i <= column + 1; i++)
				if (i >= 0 && j >= 0 && i < columns && j < rows && !(j == row && i == column))
					yield return arr[j][i];
		}

		public LatticeGraph(IReadOnlyCollection<Coordinate> area, IReadOnlyCollection<LineString> roadLines,
			IReadOnlyCollection<LineString> rivers, List<Polygon> buildingPoints)
		{
			var areaTop = area.Max(x => x.Y);
			var areaBottom = area.Min(x => x.Y);
			var areaRight = area.Max(x => x.X);
			var areaLeft = area.Min(x => x.X);
			//get graph vertices 
			for (var i = areaBottom; i <= areaTop; i += GridFrequency)
			{
				var pointsRow = new List<Node>();
				var j = areaLeft;
				for (; j <= areaRight; j += GridFrequency)
				{
					pointsRow.Add(new Node(new Coordinate(j, i)));
				}

				_NodesMatrix.Add(pointsRow);
			}

			//find blocked edges
			for (var i = 0; i < _NodesMatrix.Count; i++)
			{
				for (var j = 0; j < _NodesMatrix[i].Count; j++)
				{
					var vertex = _NodesMatrix[i][j];
					var nearby = AdjacentBottomRightElements(_NodesMatrix, i, j).ToList();

					foreach (var neighbor in nearby)
					{
						var edgeLine = new LineString(new[] {vertex.Coordinate, neighbor.Coordinate});

						var barrierFound = buildingPoints.Any(building => edgeLine.Intersects(building));

						foreach (var river in rivers)
						{
							if (barrierFound)
								break;

							if (!edgeLine.Intersects(river)) continue;
							barrierFound = true;
							break;
						}

						if (barrierFound) continue;
						double cost = 1;

						if (roadLines.Any(line => edgeLine.Intersects(line)))
						{
							cost = 4;
						}

						var edge = new Edge(vertex, neighbor, cost * edgeLine.Length);
						vertex.Connections.Add(edge);
						neighbor.Connections.Add(edge);
					}
				}
			}

			Nodes = _NodesMatrix.SelectMany(x => x).ToList();

			_BuildingPolygons = buildingPoints;
		}

		public override Node GetClosestNode(Coordinate sourcePoint)
		{
			return Nodes.OrderBy(node => new LineString(new[] {node.Coordinate, sourcePoint}).Length).First(n =>
				n.Connections.Count != 0 && _BuildingPolygons.TrueForAll(b => !b.Intersects(new Point(n.Coordinate))));
		}
	}
}