﻿using NetTopologySuite.Geometries;
using OSMLSGlobalLibrary.Modules;
using System.Linq;
using CityDataExpansionModule.OsmGeometries;

namespace PathsFindingCoreModule
{
	[CustomInitializationOrder(-1000)]
	public class PathsFindingCore : OSMLSModule
	{
		protected override void Initialize()
		{
			var allPoints = MapObjects
				.GetAll<Point>()
				.Select(x => x.Coordinate)
				.ToList();

			var roads = MapObjects
				.GetAll<OsmWay>()
				.Where(x => x.Tags.ContainsKey("highway"))
				.OrderBy(x => x.Coordinate.X).ThenBy(x => x.Coordinate.Y)
				.Cast<LineString>()
				.ToList();

			var buildings = MapObjects
				.GetAll<OsmClosedWay>()
				.Where(x => x.Tags.ContainsKey("building"))
				.Select(x => x.ToPolygon)
				.ToList();

			var waterways = MapObjects
				.GetAll<OsmWay>()
				.Where(x => x.Tags.ContainsKey("waterway"))
				.OrderBy(x => x.Coordinate.X).ThenBy(x => x.Coordinate.Y)
				.Cast<LineString>()
				.ToList();

			PathsFinding.AddGraph(new LatticeGraph(allPoints, roads, waterways, buildings), "Walking");
			PathsFinding.AddGraph(new CommonGraph(roads), "Transport");
		}

		public override void Update(long elapsedMilliseconds)
		{
		}
	}
}