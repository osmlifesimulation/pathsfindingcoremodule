﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace PathsFindingCoreModule
{
	public abstract class Graph
	{
		public List<Node> Nodes = new List<Node>();
		public abstract Node GetClosestNode(Coordinate sourcePoint);
	}

	public class Node
	{
		public Coordinate Coordinate { get; }
		public List<Edge> Connections { get; } = new List<Edge>();

		public Node(Coordinate point)
		{
			Coordinate = point;
		}

		public Node ConnectedNode(Edge edge)
		{
			return Connections.Find(e => e == edge).AnotherNode(this);
		}
	}

	public class Edge
	{
		public double Cost { get; }
		private Node[] ConnectedNodes { get; } = new Node[2];

		public Edge(Node first, Node second, double cost)
		{
			ConnectedNodes[0] = first;
			ConnectedNodes[1] = second;
			Cost = cost;
		}

		public Node AnotherNode(Node node)
		{
			return ConnectedNodes.First(n => n != node);
		}
	}
}