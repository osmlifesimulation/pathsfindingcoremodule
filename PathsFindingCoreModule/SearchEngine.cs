﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PathsFindingCoreModule
{
	public class SearchEngine
	{
		private class NodeDescriptor
		{
			public double? MinCostToStart { get; set; }
			public Node NearestToStart { get; set; }
			public bool Visited { get; set; }
			public double StraightLineDistanceToEnd { get; set; }
		}

		private Node Start { get; set; }
		private Node End { get; set; }

		private readonly Dictionary<Node, NodeDescriptor> _Nodes;

		public SearchEngine(IEnumerable<Node> nodes)
		{
			_Nodes = nodes.ToDictionary(n => n, n => new NodeDescriptor());
		}

		public void ChangeStartEnd(Node start, Node end)
		{
			Start = start;
			End = end;
		}

		private void BuildShortestPath(ICollection<Node> list, Node node)
		{
			while (true)
			{
				if (_Nodes[node].NearestToStart == null) return;
				list.Add(_Nodes[node].NearestToStart);
				node = _Nodes[node].NearestToStart;
			}
		}

		public List<Node> GetShortestPathAstart()
		{
			foreach (var (node, descriptor) in _Nodes)
			{
				descriptor.StraightLineDistanceToEnd = StraightLineDistance(node, End);
			}

			AstarSearch();
			var shortestPath = new List<Node> {End};
			BuildShortestPath(shortestPath, End);
			shortestPath.Reverse();
			return shortestPath;
		}

		private static double StraightLineDistance(Node from, Node to)
		{
			return Math.Sqrt(Math.Pow(from.Coordinate.X - to.Coordinate.X, 2) +
			                 Math.Pow(from.Coordinate.Y - to.Coordinate.Y, 2));
		}

		private void AstarSearch()
		{
			_Nodes[Start].MinCostToStart = 0;
			var prioQueue = new List<Node> {Start};
			do
			{
				prioQueue = prioQueue.OrderBy(x => _Nodes[x].MinCostToStart + _Nodes[x].StraightLineDistanceToEnd)
					.ToList();
				var node = prioQueue.First();
				prioQueue.Remove(node);

				foreach (var cnn in node.Connections.OrderBy(x => x.Cost))
				{
					var childNode = node.ConnectedNode(cnn);
					var childNodeDescription = _Nodes[childNode];
					if (childNodeDescription.Visited)
						continue;
					if (childNodeDescription.MinCostToStart != null && !(_Nodes[node].MinCostToStart + cnn.Cost <
					                                                     childNodeDescription.MinCostToStart)) continue;
					childNodeDescription.MinCostToStart = _Nodes[node].MinCostToStart + cnn.Cost;
					childNodeDescription.NearestToStart = node;
					if (!prioQueue.Contains(childNode))
						prioQueue.Add(childNode);
				}

				_Nodes[node].Visited = true;
				if (node == End)
					return;
			} while (prioQueue.Any());
		}
	}
}